@extends("layout.index")
@section("content")  
            <div class="right_col" role="main">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Report
                            <small>Add new</small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(count($errors)>0)
                            @foreach($errors->all() as $er)
                                <div class="alert alert-warning">
                                    <strong>Thông báo: </strong>{{$er}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-12" style="padding-bottom:120px">
                       {!! Form::open( ['url' => "report/$report->id", 'method' => 'PATCH', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
                            <div class="form-group">
                                <label>Name</label>
                                {!! Form::text('name',$report->name, array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                {!! Form::textarea('content',$report->content, array('class' => 'form-control ', 'rows' => 3)) !!}
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i> Update</button>
                       {!! Form::close() !!}
                    </div>
                </div>
            </div>
@endsection
