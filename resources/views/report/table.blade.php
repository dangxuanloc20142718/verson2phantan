<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	                        <thead>
	                            <tr align="center">
	                                <th>STT</th>
	                                <th>Detail Report</th>
	                                <th>Description</th>
	                                <th>Role</th>
	                                <th>Authorize</th>
	                                <th>Share</th>
	                                <th>Delete</th>
	                                <th>Update</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	@foreach($report as $key => $val)
	                            <tr class="odd gradeX" align="center">
	                                <td>{{$key+1}}</td>
	                                <td><a href="conversation/{{$val->id}}" style="color: red">{{$val->name}}</a></td>
	                                <td>{!!$val->content!!}</td>
	                                <td>
	                                	@if($val->permission == 0)
	                                		Read
	                                	@elseif($val->permission == 1)
											R & W
	                                	@else
											Admin
	                                	@endif
	                                </td>
	                                <td>
	                                	@if($val->permission == 2)
	                                		<a href="authorize/{{$val->id_report}}"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-users"></i> Authorize</button></a>
	                                	@endif
	                                </td>
	                                <td>
	                                	@if($val->permission == 2)
	                                		<a href="share_authorize/{{$val->id_report}}" class="btn btn-primary btn-sm"><i class="fa fa-share-alt"></i> Share</a>
	                                	@endif
	                                </td>
	                                <td>
	                                	@if($val->permission == 2)
	                                	<form action="report/{{$val->id}}" method="POST">
									    	<input type="hidden" name="_method" value="DELETE">
									    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
									   		<button type="submit" class="btn btn-primary btn-sm xoa"><i class="fa fa-trash"></i> Delete</button>
										</form>
										@endif
	                                </td>
	                                  
	                                <td class="center">
	                                	@if($val->permission == 2 || $val->permission == 1)
	                                	<a href='report/{{$val->id}}/edit'><button type="button" class="btn btn-primary btn-sm">
	                                	<i class="fa fa-pencil"></i> Update
	                                	</button></a>
	                                	@endif
	                                </td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>

<script type="text/javascript">
	$(".xoa").click(function(event){
			if(!confirm("bạn có chắc chắn muốn xóa không")){
				return false;
			}

	});
</script>
	                    