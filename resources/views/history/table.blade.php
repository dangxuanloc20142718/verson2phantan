		<table class="table table-striped table-bordered table-hover" id="dataTables-example">

	                        <thead>
	                            <tr align="center">
	                                <th>STT</th>
	                                <th>Name</th>
	                                <th>Content edit</th>
	                                <th>Time edit</th>

	                                @if($permission == 2 || $permission == 1)
	                             	<th>Restore</th>
	                             	@endif

	                             	@if($permission == 2)
	                             	<th>Delete</th>
	                             	@endif

	                            </tr>
	                        </thead>

	                        <tbody>
	                        	@foreach($history as $key => $val)
	                            <tr class="odd gradeX" align="center">
	                                <td>{{$key+1}}</td>
	                                <td>{{$val->name}}</td>
	                                <td>{!!$val->content_edit!!}</td>
	                                <td>{{$val->updated_at}}</td>

	                                @if($permission == 2 || $permission == 1)
	                                <td><a href="restore/{{$val->id}}" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i> Restore</a></td>
									@endif

	                                @if($permission == 2)
	                                <td><a href="delete_history/{{$val->id}}" class="btn btn-primary delete"><i class="fa fa-trash"></i> Delete</a></td>
	                                @endif

	                            </tr>                               
	                            @endforeach
	                        </tbody>
		</table> 
					    <div class="col-lg-12">
                        	<p style="text-align: right;"><a href="delete_all_history/{{$id_conversation}}" class="btn btn-primary delete"><i class="glyphicon glyphicon-trash"></i> Delete all</a></p>
                    	</div>

<script type="text/javascript">
	$('.delete').click(function(){
		if(!confirm('Bạn có chắc chắc muốn xóa Conversation này không')){
			return false;
		}
	});
</script>
 
       