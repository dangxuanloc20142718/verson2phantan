@extends("layout.index")
@section("content")  
   <div class="right_col" role="main">
            <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Conversation
                            <small>Edit</small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <div class="col-lg-2" >      
                         {!! Form::open( ['url' => "conversation/$id_report", 'method' => 'GET', 'class' => 'form-horizontal selectsearch', 'name'=>'uploadform', 'files'=>true] ) !!}
                            <div class="form-group">
                                  {!! Form::select('id_user_select',$list_username,$id_user_select,array('class' => 'form-control select')) !!}
                            </div>
                          {!! Form::close() !!}
                    </div>  
                    <div class="col-lg-4">
                          {!! Form::open( ['url' => "conversation/$id_report", 'method' => 'GET', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
                            <label><input type="search" class="form-control input-sm" placeholder="hh:mm:ss or content" name="keyword"></label>
                            <button type="submit" class="btn btn-sm btn-primary search" style="margin-top:3px"><i class="fa fa-fw fa-search"></i> Search</button>
                          {!! Form::close() !!}
                    </div>              
                        @include('conversation.table')
              </div>
  </div>

<script type="text/javascript">
      $('.select').change(function(){
            $('.selectsearch').submit();
      });
</script>            
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://js.pusher.com/3.1/pusher.min.js"></script>
<script>
      //instantiate a Pusher object with our Credential's key
      var pusher = new Pusher('2c618c6614c7cd6214b9', {
          cluster: 'ap1',
          encrypted: true
      });

      //Subscribe to the channel we specified in our Laravel Event
      var channel = pusher.subscribe('my-channel');

      //Bind a function to a Event (the full Laravel class)
      channel.bind('App\\Events\\SendMessageEvent', addMessage);

      function addMessage(data) {
        var id=data.message.id;
        CKEDITOR.instances[id].setData(data.message.content);
      }
    </script>
    <!-- <script type="text/javascript">
        $(document).ready(function(){
            $('.inputHoithoai').change(function(){
                 $.ajaxSetup({
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                 });
                $.ajax({
                    url: 'conversation',
                    data: {content: $(this).val(),
                           id: $(this).attr('name')
                    },
                    type: 'post'
                });  
               
            });
           
            
        });  
    </script> -->
@endsection