@extends("layout.index")
@section("content")  
   <div class="right_col" role="main">
            <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Report
                            <small>{{$report->name}}</small>
                            <p style="text-align: right;"><small><a class="btn btn-primary" href="change_template/{{$report->id}}"><i class="fa fa-file-code-o"></i> Change template</a></small></p>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <center style="margin-top: 20px">
                        <div class="col-lg-12" style="padding-bottom:120px">
                            {!! Form::open( ['url' => "exportfile", 'method' => 'post', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
                            <div class="form-group" style="margin-top: 30px">
                                <h1><label>Content</label></h1>
                                @if($role == 0)
                                    {!!Form::textarea('fullcontent',$report->fullcontent, array('class' => 'form-control','disabled'=>'disabled','id' => 'edit4', 'rows' => 20,
                                    )) !!}
                                @else
                                    {!!Form::textarea('fullcontent',$report->fullcontent, array('class' => 'form-control','id' => 'edit4', 'rows' => 20
                                    )) !!}
                                @endif
                            </div>                         

                            <div class="form-group">
                                 @if($role == 0)
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
                                    <a href="update_content_report/{{$report->id}}" class="btn btn-primary update hidden"><i class="fa fa-pencil"></i> Update</a>
                                 @else
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
                                    <a href="update_content_report/{{$report->id}}" class="btn btn-primary update"><i class="fa fa-pencil"></i> Update</a>
                                 @endif
                            </div>
                             {!! Form::close() !!}
                         
                        </div>
                    </center>
              </div>
  </div>
  <script type="text/javascript">
            config = {};
            config.language ='en';
            config.width = '650px';
            config.height = '800px';
            CKEDITOR.replace('edit4',config);
 </script>  
 <script type="text/javascript">
            $('.update').click(function(){
                if(!confirm('Khi bạn chọn chức năng này,toàn bộ nội dung bản báo cáo sẽ được update theo trang edit,bạn có chắc muốn update không?')){
                    return false;
                }
            });
 </script>
 <script src="https://js.pusher.com/3.1/pusher.min.js"></script>
 <script>
      //instantiate a Pusher object with our Credential's key
      var pusher = new Pusher('2c618c6614c7cd6214b9',{
          cluster: 'ap1',
          encrypted: true
      });

      //Subscribe to the channel we specified in our Laravel Event
      var channel = pusher.subscribe('my-channel');

      //Bind a function to a Event (the full Laravel class)
      channel.bind('App\\Events\\SendMessageEvent', addMessage);

      function addMessage(data) {
            CKEDITOR.instances['edit4'].setData(data.message.fullcontent);
      }
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            CKEDITOR.instances.edit4.on('blur', function(){ 
                $.ajaxSetup({
                    headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                 });
                $.ajax({
                    url: 'update_realtime_content_report',
                    data: {content: CKEDITOR.instances['edit4'].getData(),
                           id_report: <?php echo $report->id ?>
                    },
                    type: 'post',
                });  

            });
         });
    </script>
            
@endsection


