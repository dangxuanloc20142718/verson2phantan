<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>DSD_10</title>
    <base href="{{asset('')}}">
    <!-- Bootstrap -->
   <!-- Bootstrap -->
    <link href="template1/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="template1/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="template1/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="template1/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="template1/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="template1/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="template1/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="template1/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="template1/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="template1/build/css/custom.min.css" rel="stylesheet">
     <!-- jQuery -->
    <script src="template1/vendors/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="js/jspdf.min.js"></script>

    <style type="text/css">
        th {
            text-align: center;
        }
        td {
            text-align: center;
        }
        .page-header {
            margin-top: 5px;
        }
    </style>
    
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
         
        <!-- sidebar -->
        @include('layout.sidebar')
        <!-- /sidebar -->

        <!-- top navigation -->
        @include('layout.navigation')
        <!-- /top navigation -->

        <!-- page content -->
        @yield('content') 
        <!-- /page content -->

        <!-- footer content -->
        @include('layout.footer')
        <!-- /footer content -->
       </div>
    </div>
    <!-- Bootstrap -->
    <script src="template1/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="template1/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="template1/vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="template1/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- iCheck -->
    <script src="template1/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->1
    <script src="template1/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="template1/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="template1/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="template1/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="template1/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="template1/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="template1/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="template1/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="template1/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="template1/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="template1/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="template1/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="template1/vendors/jszip/dist/jszip.min.js"></script>
    <script src="template1/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="template1/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="template1/build/js/custom.min.js"></script>

    
  </body>
</html>