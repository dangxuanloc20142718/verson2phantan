<div class="col-lg-12" style="padding-bottom:120px">
                       <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr align="center">
                                    <th>STT</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user_per as $key => $val)
                                     @if($val->id_user != Session::get('user')->id)
                                        <tr class="odd gradeX" align="center">
                                            <td>{{$key+1}}</td>
                                            <td>{{$val->name}}</td>
                                            <td>{{$val->email}}</td>
                                            <td>
                                                <a href="delete_share_authorize/{{$val->id_user}}/{{$id_report}}" class=" btn btn-primary delete
                                                    @if(in_array($val->id_user,$array_id_user_in_conversation))
                                                            hidden
                                                    @endif "
                                                ><i class="fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>

<script type="text/javascript">
    $('.delete').click(function(){
         if(!confirm('Bạn có chắc chắn muốn xóa phân quyền cho người này không ?')){
            return false;
         }
    });
</script>