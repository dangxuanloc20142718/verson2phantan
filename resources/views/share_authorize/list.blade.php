@extends('layout.index')
@section('content')
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Table Share<small>List</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12">
                                @if(count($errors)>0)
                                    @foreach($errors->all() as $er)
                                        <div class="alert alert-warning">
                                            <strong>Thông báo: </strong>{{$er}}
                                        </div>
                                    @endforeach
                                @endif
                    </div>
                    <div class="row ">
                                <div class="col-lg-6" style="padding-bottom:15px;padding-left:20px">
                                    <div style="text-align:left">
                                    
                                        <label><input type="search" class="form-control input-sm" placeholder="abc@gmail.com" name="keyword"></label>
                                        <button class="btn btn-sm btn-primary search" style="margin-top:3px"><i class="fa fa-fw fa-search"></i> Search</button>
                                    </div>
                                </div>
                                <div id='search'>       
                                </div>
                    </div>
                    <div class="col-lg-12">
                                @if(Session::has('message'))
                                    <br/>
                                    <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                                @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="x_content"> 
                    @include('share_authorize.table');
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <script type="text/javascript">
            $('.search').click(function(){
                $.ajaxSetup({
                        headers:{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                     });
                $.ajax({
                        url:'share_authorize',
                        type: 'post',
                        data: {key_search : $("[type='search']").val(),
                               id_report  : {!! $id_report !!}
                        },
                        success: function(data){
                            $('#search').html(data.html);
                        }
                     });
            });
        </script>
@endsection