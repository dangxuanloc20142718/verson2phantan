@extends("layout.index")
@section("content")
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Table Template<small>List</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <div class="x_content"> 
                        @include("template.table")
                    </div>
                    <div class="col-lg-12">
                        <p style="text-align: right;"><a href="delete_all_template"><button type="button" class="btn btn-primary delete_all"><i class="glyphicon glyphicon-trash"></i> Delete all</button></a></p>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
          

        <script type="text/javascript">
            $('.delete_all').click(function(){
                if(!confirm("bạn có chắc chắn muốn xóa hết")){
                    return false;
                }
            });
        </script>
@endsection
