<table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                        <thead>
	                            <tr align="center">
	                                <th>STT</th>
	                                <th>Name</th>
	                                <th>Description</th>
	                                <th>Delete</th>
	                                <th>Update</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	@foreach($list_temp as $key => $val)
	                            <tr class="odd gradeX" align="center">
	                                <td>{{$key+1}}</td>
	                                <td>{{$val->name}}</td>
	                                <td>{!!$val->content!!}</td>
	                                <td class="center">
	                                	<form action="template/{{$val->id}}" method="POST">
									    	<input type="hidden" name="_method" value="DELETE">
									    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
									   		<button type="submit" class="btn btn-primary btn-sm xoa"><i class="fa fa-trash"></i> Delete</button>
										</form>
	                                </td>
	                                <td class="center">
	                                	<a href='template/{{$val->id}}/edit'><button type="button" class="btn btn-primary btn-sm">
	                                	<i class="fa fa-pencil"></i> Update
	                                	</button></a>
	                                </td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>

<script type="text/javascript">
	$(".xoa").click(function(event){
			if(!confirm("bạn có chắc chắn muốn xóa không")){
				return false;
			}

	});
</script>
	                    