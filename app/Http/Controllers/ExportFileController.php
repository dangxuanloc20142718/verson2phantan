<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Elibyy\TCPDF\Facades\TCPDF;

class ExportFileController extends Controller
{
    public function postExportFile(Request $request){
    	$content = $request->fullcontent;
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf::SetFont('freeserif', '', 10, '', false);
        $pdf::AddPage();   
        $pdf::writeHTML($content, true, false, false, false, '');
        $pdf::Output('hello_world.pdf');
    }
}
