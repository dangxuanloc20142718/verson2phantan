<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dialog;
use App\DetailDialog;
use App\Permission;
use App\User;
use DB;
use Session;

class DialogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $dialog=DB::table('users')
       //  ->join('tbl_permission','tbl_permission.id_user','=','users.id')
       //  ->join('tbl_dialog','tbl_dialog.id','=', 'tbl_permission.id_dialog')
       //  ->where('users.id','=',1)
       //  ->get();
        $dialog=Dialog::all();

        return view('admin.dialog.list',compact('dialog'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dialog.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $infor = $request->all();
        $dialog = new Dialog();
        $id=$dialog->create($infor)->id;
        $permission=new Permission();
        $id_user=$permission->id_user=1;
        $id_dialog=$permission->id_dialog=$id;
        $infor=['id_user'=>$id_user,'id_dialog'=> $id_dialog,'permission'=> 2];
        $id_per=$permission->create($infor)->id;

        if($request->hasFile('txt')){
            $file=$request->file('txt');
            $tail=$file->getClientOriginalExtension();
            if($tail != 'txt'){
                return redirect('dialog/create')->with('message','file bạn nhập không phải là file .txt');
            }
            $name=$file->getClientOriginalName();
            $new_name=str_random(4).'_'.$name;  
            while(file_exists('upload/sanpham'. $new_name)){
                 $new_name=str_random(4).'_'.$name;  
            }    
            $file->move('upload/sanpham',$new_name);
            $fileOpen=@fopen('upload/sanpham/'.$new_name,'r');
            $contents="";
            while(!feof($fileOpen)){
                $contents= $contents.trim(fgets($fileOpen));
            }
            $mang=explode('[',$contents);
            for($i=1;$i < count($mang);$i++){
                $detail = new DetailDialog();
                $vitri=strpos($mang[$i],'+');
                $detail->id_dialog=$id_per;
                $detail->id_user=substr($mang[$i],9,$vitri-9);
                $detail->time=substr($mang[$i],$vitri+6,19);
                $detail->content=trim(substr($mang[$i],$vitri+34),']');
                $detail->save();
            }
            
        }
        Session::flash('message','bạn vừa tạo mới một cuộc hội thoại');
        return redirect('dialog/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
