<?php

namespace App\Http\Controllers;
use App\Http\Requests\ReportRequest;
use Illuminate\Http\Request;
use App\Report;
use App\Permission;
use App\Conversation;
use App\History;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;


class ReportController extends Controller
{
      public function index()
    {
    	$id_user=Auth::id();
        $report=DB::table('users')
        ->join('permission','permission.id_user' , 'users.id')
        ->join('report','report.id' , 'permission.id_report')
        ->where('users.id' , $id_user)
        ->get();
        return view('report.list',compact('report'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('report.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReportRequest $request)
    {
        $infor = $request->all();
        $report = new Report();
        $id_report = $report->create($infor)->id;
        if($request->hasFile('file1') && $request->hasFile('file2') && $request->hasFile('file3')){
            $file1 = $request->file('file1');
            $file2 = $request->file('file2');
            $file3 = $request->file('file3');
            $content1 = content_file($file1);
            $content2 = content_file($file2);
            $content3 = content_file($file3);
            $array1 = arrays_string($content1);
            $array2 = arrays_string($content2);
            //merg file 1 với file 2
            $content_merg = '';
            foreach ($array1 as $val1) {
                foreach ($array2 as $val2) {
                    if(get_time_start($val1) == get_time_start($val2) && get_audio_frequency($val1) == get_audio_frequency($val2)){
                        $content_merg .= '[id_user:'.get_content_or_id($val1).'+time:'.get_time_start($val1).'+'.get_time_finish($val1).'+content:'.get_content_or_id($val2).'+ACC:'.get_audio_frequency($val1).'+label:'.get_label($val1).']';
                    }
                }
            }
            $array_merg = arrays_string($content_merg);
            $array_stenograph = arrays_string($content3);
            // $contents = '';
            // foreach ($arr1 as  $id_time) {
            //     foreach ($arr2 as $cont_time) {
            //         if(getTime_or_Id_or_Content($id_time,1) == getTime_or_Id_or_Content($cont_time,1)){
            //             $id = getTime_or_Id_or_Content($id_time,0);
            //             $time = getTime_or_Id_or_Content($id_time,1);
            //             $content = getTime_or_Id_or_Content($cont_time,0);
            //             $string = '[id_user:'.$id.'+time:'.$time.'+content:'.$content.']';
            //             $contents .= $string;
            //         }
            //     }
            // }
            // $array = explode('[',$contents);
            $collection = [];// mảng các mảng id-user-time-content
            $id_list=[];//mảng các id_user
            for ($i=0; $i < count($array_merg); $i++) { 
                for ($j=0; $j < count($array_stenograph); $j++) { 
                    if(get_content_or_id($array_merg[$i]) == get_content_or_id($array_stenograph[$j]) && get_label_final($array_merg[$i]) == get_label_final($array_stenograph[$j])){
                        $id_user = get_content_or_id($array_merg[$i]);
                        $time_start = get_time_start($array_merg[$i]);
                        $time_finish = get_time_finish($array_merg[$i]);
                        $cont_merg = get_content_merg($array_merg[$i]);
                        $cont_stenograph = get_content_stenograph($array_stenograph[$j]);
                        $content = compare_content($cont_merg,$cont_stenograph);
                        $collection[] = ['id_user' =>  $id_user,
                                'time' => $time_start,
                                'time_end' => $time_finish,
                                'content'=> $content];
                        $id_list[] = $id_user;
                        
                }
            }
            // for($i = 1;$i < count($array);$i++){
            //     $vitri = strpos($array[$i],'+');
            //     $id_user = substr($array[$i],8,$vitri-8);
            //     $time = substr($array[$i],$vitri+6,19);
            //     $content = trim(substr($array[$i],$vitri+34),']');
            //     $collection[] = ['id_user' =>  $id_user,
            //      				'time' => $time,
            //       				'content'=> $content];
            //     $id_list[] = $id_user;
            // }   
            }
        }
        //tạo các permission
        		$id_user = Session::get('user')->id;
        		$id_list[] = $id_user;
        	foreach (array_unique($id_list) as  $id) {
        		$permission = new Permission();
        		$permission->id_user = $id;
        		$permission->id_report =  $id_report;
        		if($id == $id_user){
        			$permission->permission = 2;
        		}
        		$permission->save();
        	}
        //tạo các conversation
        	   foreach ($collection as $colect) {
        	   	   $permis = DB::table('permission')->where([
        	   	   			['id_user', '=' , $colect['id_user']],
        	   	   			['id_report', '=', $id_report]
        	   	    ])->get();
        	   	   $id_per = $permis[0]->id;
        	   	   $conversation = new Conversation();
        	   	   $conversation->id_permission = $id_per;
        	   	   $conversation->time = $colect['time'];
                   $conversation->time_end = $colect['time_end'];
        	   	   $conversation->content = $colect['content'];
        	   	   $conversation->save();
        	   }
       Session::flash('message','Bạn đã tạo mới thành công một cuộc hội thoại');
       return redirect('report/create');

      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = Report::find($id);
        return view('report.update',compact('report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $data = $request->all();
       $report = Report::find($id);
       $report->update($data);
       Session::flash('message',"Bạn đã cập nhật thành công");
       return redirect("report/$id/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
    	$id_user=Session::get('user')->id;
        $report = Report::find($id);
        $report->delete();
        $permission = Permission::where('id_report',$id)->get();
        $id_per_list = [];
        foreach ($permission as  $per) {
        	$id_per_list[] = $per->id;
        	$per->delete();
        }
        foreach ($id_per_list as $val) {
        	$conversation = Conversation::where('id_permission',$val)->get();
        	foreach ($conversation as $value){
                $id_cover = $value->id;
                $history_list = History::where('id_conversation',$id_cover);
                foreach ($history_list as  $val) {
                    $val->delete();
                }
        		$value->delete();
        	}
        }
        Session::flash('message',"Bạn đã xóa thành công");
        return redirect('report');       
    }
}
