<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Permission;
use DB;
use Session;

class AuthorizeController extends Controller
{
    public function getAuthorize($id){
    	$id_report = $id;
    	$user_per = DB::table('users')
    		->join('permission','permission.id_user','users.id')
    		->where('id_report',$id)
    		->get();
    	return view('authorize.list',compact('user_per','id_report'));
    }
    public function postAuthorize(Request $request){
    	$array_id = $request->all();
    	$id_report = $array_id['id_report'];
    	foreach ($array_id as $key => $value) {
    		if($key == '_token' || $key == 'id_report'){
    			continue;
    		}
    		$colect_per = DB::table('permission')->where([['id_user',$key],['id_report',$id_report]])->update(['permission' => $value]);
    	}	
    	Session::flash('message','Bạn đã phân quyền thành công');
    	return redirect("authorize/$id_report");
	}
}
