<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SocialAccountService;
use Illuminate\Support\Facades\Log;
use Socialite;
use App\User;

class SocialAuthController extends Controller
{
     public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback()
    {
        $user = Socialite::driver('facebook')->user();
        dd($user);
    }
    public function addnew(Request $request){
    	$user = new User();
    	$user->name = $request->name;
    	$user->password = $request->password;
    	$user->email = $request->email;
    	$user->save();
    }
}
