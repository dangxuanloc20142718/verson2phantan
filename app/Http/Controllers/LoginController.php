<?php

namespace App\Http\Controllers;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class LoginController extends Controller
{
    public function getLogin(){
      if(Auth::check()){    
          return redirect('report');
      }else{       
    	    return view('login');
      }
    }
    public function postLogin(LoginRequest $request){
      $data = ['email' => $request->email,
              'password' => $request->password];
  		if(Auth::attempt($data)){
        Session::put('user',Auth::user());
  			return redirect('report');
  		}else{
        Session::flash('message',"Tài khoản hoặc mật khẩu bạn nhập không đúng");
  			return redirect('getlogin');
  		}
    }
}
