<?php

namespace App\Http\Controllers;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use App\User;
use Session;

class RegisterController extends Controller
{
    public function getRegister(){
    	return view('register');
    }
    public function postRegister(RegisterRequest $request){
    	$name = $request->name;
    	$email = $request->email;
    	$password = bcrypt($request->password);
    	$data = ['name' => $name, 'email' => $email, 'password' => $password];
    	$user = new  User();
    	$user->create($data);
    	Session::flash('message','Bạn đã đăng kí thành công');
    	return redirect('getlogin');
    }
}
