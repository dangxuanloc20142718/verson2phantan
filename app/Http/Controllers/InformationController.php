<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\User;
use Hash;

class InformationController extends Controller
{
    public function getInformation(){
        $user = Session::get('user');
        return view('information.list',compact('user'));
    }
    public function postInformation(Request $request){
        $id_user = Session::get('user')->id;
        $user = User::find($id_user);
        if($request->has('check')){
            if(Hash::check($request->pass_old,$user->password)){
                if($request->pass_new == $request->pass_confirm){
                    $user_update = User::find($user->id);
                    $user->name = $request->name;
                    $user->password = Bcrypt($request->pass_new);
                    $user->save();
                    Session::put('user',$user);
                    Session::flash('message','Bạn đã cập nhật thành công');
                    return redirect('information');
                }else{
                    Session::flash('message','Password confirm không chính xác');
                    return redirect('information');
                }
            }else{
                Session::flash('message','Password cũ không đúng');
                return redirect('information');
            }
        }else{
            $user_update = User::find($user->id);
            $user_update->name = $request->name;
            $user_update->save();
            $user = Session::get('user');
            $user->name = $request->name;
            Session::put('user',$user);
            Session::flash('message','Bạn đã cập nhật thành công');
            return redirect('information');
        }
    }
}