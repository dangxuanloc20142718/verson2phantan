<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template;
use Session;

class DeleteAllTemplateController extends Controller
{
    public function deleteAll(){
    	$id_user = Session::get('user')->id;
    	$list_template = Template::where('id_user',$id_user)->get();
    	foreach ($list_template as $value) {
    		$template = Template::find($value->id);
    		$template->delete();
    	}
    	Session::flash('message','Bạn vừa xóa hết tất cả template');
    	return redirect("template");
    }
}
