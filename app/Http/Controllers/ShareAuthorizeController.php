<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Permission;
use App\Conversation;
use DB;
use Session;

class ShareAuthorizeController extends Controller
{
    public function getShare($id_report){
    	$user_per = DB::table('users')
    		->join('permission','permission.id_user','users.id')
    		->select('users.name as name','users.email as email','permission.id as id','permission.id_user as id_user')
    		->where('id_report',$id_report)
    		->get(); 
        //lấy mảng các id có trong cuộc hội trong cuộc hội thoại 
    	$array_id_user_in_conversation = [];
    	foreach ($user_per as $value) {
    		$test = DB::table('conversation')->where('id_permission',$value->id)->get();
    		if(count($test) > 0){
    			$array_id_user_in_conversation[] = $value->id_user;
    		}
    	}
    	return view('share_authorize.list',compact('user_per','id_report','array_id_user_in_conversation'));
    }
    public function searchUser(Request $request){
    	if($request->ajax()){
    		$key_search = $request->key_search;
    		$id_report = $request->id_report;
    		$users = DB::table('users')->where('email','like','%'.$key_search.'%')->get();
    		return Response::json([
    			 'html' => view('share_authorize.table_search')
    			 	->with('users',$users)
    			 	->with('id_report',$id_report)
                    ->render(),
    		]);
    		return $users;
    	}
    }
 
    public function addAuthorize($id_user,$id_report){
		$user = DB::table('permission')->where([['id_user',$id_user],['id_report',$id_report]])->get();
		//kiểm tra xem user thêm vào tồn tại hay chưa
		if(count($user) == 0){
			$permission = new Permission();
			$permission->id_user = $id_user;
			$permission->id_report = $id_report;
			$permission->permission = 0;
			$permission->save();
			Session::flash('message','Bạn vừa phân quyền cho một người mới trong cuộc hội thoại này');
			return redirect("share_authorize/$id_report");
		}
		Session::flash('message','Người này đã được phân quyền trước đó');
		return redirect("share_authorize/$id_report");
	}
	public function deleteAuthorize($id_user,$id_report){
		   DB::table('permission')->where([['id_user',$id_user],['id_report',$id_report]])->delete();
		   Session::flash('message','Bạn vừa xóa thành công');
		   return redirect("share_authorize/$id_report");
	}

    
}
