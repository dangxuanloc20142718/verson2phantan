<?php

namespace App\Http\Controllers\Admin;
use App\SanPham;
use App\DanhMuc;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SanPhamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $sp=SanPham::all();
       if($request->ajax()){
            return Response::json([
                'html' => view('admin.sanpham.table')->with('sp',$sp)->render()
            ]);
       };
       return view("admin.sanpham.list",['sp'=>$sp]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $danhmuc = [];
        $dm = DanhMuc::all();
        foreach ($dm as  $value) {
            $danhmuc[$value->id] = "---$value->ten_dm---";
        }
        return view("admin.sanpham.add",compact('danhmuc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $infor=$request->all();
        if($request->hasFile('anh')){
            $file=$request->file('anh');
            $tail=$file->getClientOriginalExtension();
            if($tail != 'jpg' && $tail != 'jpeg' && $tail != 'png'){
                return redirect('admin/sanpham/create')->with('message','file bạn chọn không phải là file ảnh');
            }
            $name=$file->getClientOriginalName();
            $new_name=str_random(4).'_'.$name;  
            while(file_exists('upload/sanpham'. $new_name)){
                 $new_name=str_random(4).'_'.$name;  
            }    
            $file->move('upload/sanpham',$new_name);
            $infor['anh_sp']= $new_name;

        }else{
            $infor['anh_sp']="";
        }
        $sp=new SanPham();
        $sp->create($infor);
        return redirect('admin/sanpham/create')->with('message','Bạn đã thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sanpham = SanPham::find($id);
        $id_dm = $sanpham->id_dm;
        $danhmuc = [];
        $dm = DanhMuc::all();
        foreach ($dm as  $value) {
            $danhmuc[$value->id] = "---$value->ten_dm---";
        }
        return view("admin.sanpham.update",['sanpham' => $sanpham , 'id_dm' => $id_dm , 'danhmuc' => $danhmuc]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        echo "đặng xuân lộc";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resquest $request,$id)
    {
        $sanpham=SanPham::find($id);
        $sanpham->delete();
        $sp=SanPham::all();
        if($request->ajax()){
            return Response::json([
                'html' => view('admin.sanpham.table')->with('sp',$sp)->render()
            ]);
        };
        // return redirect('admin/sanpham')->with('message','bạn đã xóa thành công');
    }
}
