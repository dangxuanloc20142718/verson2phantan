<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Conversation;
use App\Events\SendMessageEvent;
use App\History;
use App\Permission;
use Session;

class HistoryController extends Controller
{
    public function getHistory($id){
    	//lấy danh sách history tương tứng với id_permission
    	$history = DB::table('users')
    	->join('history','history.id_user','users.id')
    	->where('id_conversation',$id)
    	->get();
        //kiểm tra quyền của user đang đăng nhập đối với tập các history
        $conversation = Conversation::find($id);
        $per_user_create = $conversation->permission;// relationship lấy thằng tạo ra 
        $id_report = $per_user_create->id_report;
        $id_user = Session::get('user')->id;
        $per_user_now = Permission::where([['id_report',$id_report],['id_user',$id_user]])->first(); //thằng đang sở hữu
        $permission =  $per_user_now->permission;
        //id_conversation
        $id_conversation =  $id;  
    	return view('history.list',compact('history','permission','id_conversation'));
    }
    public function getRestore($id){
    	$history = History::find($id);
    	$id_conver = $history->id_conversation;
    	$content_edit = $history->content_edit;
    	$convertsation = Conversation::find($id_conver);
        $old_content_conver = $convertsation->content;
    	$convertsation->content = $content_edit;
    	$convertsation->save();
        //save history trước khi restore
        $new_history = new History();
        $new_history->content_edit = $old_content_conver;
        $new_history->id_conversation = $id_conver;
        $new_history->id_user = Session::get('user')->id;
        $new_history->save();

        event(new SendMessageEvent($convertsation));
    	Session::flash('message','Bạn vừa khôi phục thành công đoạn hội thoại');
    	//lấy id_report
    	$id_permission = $convertsation->id_permission;   	
    	$id_report = Permission::find($id_permission)->id_report;
    	return redirect("conversation/$id_report");
    }
    public function deleteHistory($id){
        $history = History::find($id);
        $id_conversation =  $history->id_conversation;
        $history->delete();
        Session::flash('message','Bạn đã xóa thành công một coversation');
        return redirect("history/$id_conversation");
    }
    public function deleteAllHistory($id){
        //lấy id_report để trở lại trang
        $conversation = Conversation::find($id);
        $id_report =   $conversation->permission->id_report;
        //xóa hết
        $list_history = History::where('id_conversation',$id)->get();
        foreach ($list_history as $value) {
             $history = History::find($value->id);
             $history->delete();
        }
        Session::flash('message','Bạn đã xóa thành công');
        return redirect("conversation/$id_report");
    }
}
