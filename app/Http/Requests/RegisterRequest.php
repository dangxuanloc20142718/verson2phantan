<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'required|min:6',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'confirm' => 'same:password'
        ];
    }
    public function messages(){
        return [
            'name.required' => 'Name không được để trống',
            'name.min' => 'Name phải tối thiểu 5 kí tự',
            'email.required' => 'Email không được để trống',
            'password.required' => 'Password không được để trống',
            'password.min' => 'Password phải tối thiểu 8 kí tự',          
            'confirm.same' => 'Password confirm chưa đúng'
        ];
    }
}
