<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'content' => 'required',
            'file1' => 'required',
            'file2' => 'required',
            'file3' => 'required'
        ];
    }
    public function messages(){
        return [
            'name.required' => 'Bạn chưa nhập tên của report',
            'content.required' => 'Bạn chưa nhập mô tả báo cáo',
            'file1.required' => 'Bạn chưa chọn file1',
            'file2.required' => 'Bạn chưa chọn file2',
            'file3.required' => 'Bạn chưa chọn file3'

        ];
    }
}
