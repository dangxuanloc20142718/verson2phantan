<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function report(){
        return $this->hasMany('App\Report','id_user','id');
    }
    public function permission(){
        return $this->hasMany('App\Permission','id_user','id');
    }
    public function history(){
        return $this->hasMany('App\History','id_user','id');
    }
    
   
}
