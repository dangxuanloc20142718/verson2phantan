<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table='permission';
    protected $fillable=[
    	"id_user",
    	"id_report",
    	"permission"
    ];
    public function conversation(){
    	return $this->hasMany('App\Conversation','id_permissation','id');
    }
    public function report(){
    	return $this->belongsTo('App\Report','id_report','id');
    }
    public function user(){
    	return $this->belongsTo('App\User','id_user','id');
    }
}
